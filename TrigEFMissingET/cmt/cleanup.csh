# echo "cleanup TrigEFMissingET TrigEFMissingET-r791504 in /afs/cern.ch/work/r/rewang/track_mht/rel20/Trigger/TrigAlgorithms"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.7/CMT/v1r25p20160527
endif
source ${CMTROOT}/mgr/setup.csh
set cmtTrigEFMissingETtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtTrigEFMissingETtempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=TrigEFMissingET -version=TrigEFMissingET-r791504 -path=/afs/cern.ch/work/r/rewang/track_mht/rel20/Trigger/TrigAlgorithms  $* >${cmtTrigEFMissingETtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=TrigEFMissingET -version=TrigEFMissingET-r791504 -path=/afs/cern.ch/work/r/rewang/track_mht/rel20/Trigger/TrigAlgorithms  $* >${cmtTrigEFMissingETtempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtTrigEFMissingETtempfile}
  unset cmtTrigEFMissingETtempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtTrigEFMissingETtempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtTrigEFMissingETtempfile}
unset cmtTrigEFMissingETtempfile
exit $cmtcleanupstatus

