# echo "cleanup TrigEFMissingET TrigEFMissingET-r791504 in /afs/cern.ch/work/r/rewang/track_mht/rel20/Trigger/TrigAlgorithms"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.7/CMT/v1r25p20160527; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtTrigEFMissingETtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtTrigEFMissingETtempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=TrigEFMissingET -version=TrigEFMissingET-r791504 -path=/afs/cern.ch/work/r/rewang/track_mht/rel20/Trigger/TrigAlgorithms  $* >${cmtTrigEFMissingETtempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=TrigEFMissingET -version=TrigEFMissingET-r791504 -path=/afs/cern.ch/work/r/rewang/track_mht/rel20/Trigger/TrigAlgorithms  $* >${cmtTrigEFMissingETtempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtTrigEFMissingETtempfile}
  unset cmtTrigEFMissingETtempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtTrigEFMissingETtempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtTrigEFMissingETtempfile}
unset cmtTrigEFMissingETtempfile
return $cmtcleanupstatus

